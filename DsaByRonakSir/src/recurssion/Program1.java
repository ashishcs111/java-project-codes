package recurssion;

public class Program1 {
    public int show(int i) {
        System.out.println(i);
        if(i==10){
            return 0;
        }

        return 2*show(++i);


    }
    public static void main(String[] args) {
        Program1 p = new Program1();
        p.show(1);
    }
}
