package com.example.config;

import com.example.beans.Vehicle;

@Configuration
public class ProjectConfig {

    @Bean
    Vehicle vehicle (){
        var veh = new Vehicle();
        veh.setName("Audi 8");
        return veh;
    }
}
