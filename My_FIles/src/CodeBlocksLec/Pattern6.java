package CodeBlocksLec;

public class Pattern6 {
    public static void main(String[] args) {
        int st=5,sp=4;
        for(int i=0;i<5;i++)
        {
            for(int j=0;j<sp;j++)
            {
                System.out.print("   ");
            }
            for(int k=0;k<st;k++)
            {
                System.out.print(" * ");
            }
            System.out.println();
            st=st-1;
            sp=sp-1;
        }
    }
}
