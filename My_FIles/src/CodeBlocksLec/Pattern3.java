package CodeBlocksLec;

public class Pattern3 {
    public static void main(String[] args) {
        int st=1,sp=4;
        int c=1;
        for(int i=0;i<5;i++)
        {
            for(int j=sp;j>0;j--)
            {
                System.out.print(" ");
            }
            for(int k=0;k<st;k++)
            {
                if(c%2==0)
                {
                    System.out.print(" ! ");
                }
                else
                {
                    System.out.print(" * ");
                }
                c=c+1;
            }
            System.out.println();
            c=1;
            st=st+2;
            sp=sp-1;

        }

    }
}
