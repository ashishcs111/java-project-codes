package CodeBlocksLec;

public class Pattern2 {
    public static void main(String[] args) {
        int st=9,sp=0;
        for(int i=0;i<5;i++)
        {
           for(int j=0;j<sp;j++)
           {
               System.out.print("   ");
           }
           for(int k=st;k>0;k--)
           {
               System.out.print(" * ");
           }
            System.out.println();
           sp=sp+1;
           st=st-2;
        }
    }
}
