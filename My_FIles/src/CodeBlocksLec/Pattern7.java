package CodeBlocksLec;

public class Pattern7 {
    public static void main(String[] args) {
        int st=3,sp=1;
        for(int i=0;i<3;i++)
        {
            for(int j=0;j<st;j++)
            {
                System.out.print(" * ");
            }
            for(int k=0;k<sp;k++)
            {
                System.out.print("   ");
            }
            for(int l=0;l<st;l++)
            {
                System.out.print(" * ");
            }
            System.out.println();
            st=st-1;
            sp=sp+2;
        }
        for(int i=0;i<4;i++)
        {
            for(int j=0;j<st;j++)
            {
                System.out.print(" * ");
            }
            for(int k=0;k<sp;k++)
            {
                System.out.print("   ");
            }
            for (int l=0;l<st;l++)
            {
                System.out.print(" * ");
            }
            System.out.println();
            st=st+1;
            sp=sp-2;
        }
    }
}
