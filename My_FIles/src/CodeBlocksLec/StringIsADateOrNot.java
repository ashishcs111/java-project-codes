package CodeBlocksLec;

public class StringIsADateOrNot {
    public static void main(String[] args) {
        String str = "01/12/2000";
        if(str.matches("[0-3][0-9]/[0-1][0-9]/[0-9]{4}")==true)
        {
            System.out.println("it is a date");
        }
        else
        {
            System.out.println("it is not a date");
        }
    }
}
