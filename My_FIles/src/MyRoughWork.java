import java.util.Scanner;
public class MyRoughWork {
    public static void main(String[] args) {
/* PROGRAM FOR PERCENTAGE AND TOTAL
    public static void main(String[] args) {
        System.out.println("enter marks of the students");
        Scanner sc = new Scanner(System.in);
        System.out.println("subject 1");
        int a = sc.nextInt();
        System.out.println("subject 2");
        int b = sc.nextInt();
        System.out.println("subject 3");
        int c = sc.nextInt();
        System.out.println("subject 4");
        int d = sc.nextInt();
        System.out.println("subject 5");
        int e = sc.nextInt();

        int total = a+b+c+d+e;
        int percent = (a+b+c+d+e)/5;
        System.out.println(total);
        System.out.println(percent);
    } */


/* PROGRAM FOR TOTAL AND SUM OF THREE NUMBERS
    public static void main(String[] args) {

        System.out.println("sum of three numbers");
        int a = 5;
        int b = 6;
        int c =7;
        int sum = a+b+c;
        System.out.println(sum);

        System.out.println("another program for sum of three numbers taking input");
        Scanner sc = new Scanner(System.in);
        System.out.println("number 1");
        int d = sc.nextInt();
        System.out.println("number 2");
        int e = sc.nextInt();
        System.out.println("number 3");
        int f = sc.nextInt();

        int newsum = d+e+f;
        System.out.println(newsum);

    }*/


/* CALCULATE CGPA OF THREE SUBJECTS
    public static void main(String[] args) {
        System.out.println("Cgpa of three subjects");
        Scanner sc = new Scanner(System.in);
        System.out.println("sub 1");
        float a = sc.nextInt();
        System.out.println("sub 2");
        float b = sc.nextInt();
        System.out.println("sub 3");
        float c = sc.nextInt();

        float cgpa = (a+b+c)/30;
        System.out.println(cgpa);
    } */


/* PROGRAM TO PRINT NAME WITH HAVE A GOOD DAY
    public static void main(String[] args) {
        System.out.println("write your name here");
        Scanner sc = new Scanner(System.in);
        String str = sc.nextLine();
        //System.out.print("hello");
        //System.out.println(str);
        //System.out.println("have a good day");
        System.out.println("hello " + str +  " have a good day");
    }*/

/* PROGRAM TO CONVERT KILOMETERS INTO MILES
    public static void main(String[] args) {
        System.out.println(" enter kilometers to convert");
        Scanner sc = new Scanner(System.in);
        float a = sc.nextInt();
        float miles = a *(0.621371f);
        System.out.println(miles);
    }*/


/* PROGRAM TO FIND ENTERED NUMBER IS INTEGER OR NOT
    public static void main(String[] args) {
        System.out.println("enter the number");
        Scanner sc = new Scanner(System.in);
        System.out.println(sc.hasNextInt());

    }*/

// ARITHMETIC OPERATORS
   /* public static void main(String[] args) {
        int a = 5;
        int b = 4;
        int z = a+b;
        int y = a-b;
        int x = a*b;
        int w = a/b;
        int v = a%b;
        System.out.println(z);
        System.out.println(y);
        System.out.println(x);
        System.out.println(w);
        System.out.println(v);

        int c = ++a;
        int d = --b;
        System.out.println(c);
        System.out.println(d);
    } */

//ASSIGNMENT OPERATORS
    /* public static void main(String[] args) {
        int a = 5;
        int b = 7;    // Equal to is also an assignment operator

        a += 5;
        b *= 2;
        System.out.println(a);
        System.out.println(b);
    }  */


// COMPARISION OPERATOR
    /* public static void main(String[] args) {
        int a = 7;
        int b = 3;

        System.out.println(a==b);
        System.out.println(a>=b);    // a is greater or equal to b
        System.out.println(a<=b);    // a is smaller or equal to b

    }  */


//LOGICAL OPERATOR
   /* public static void main(String[] args) {

        int a = 6;
        int b = 3;

        System.out.println(a>=5 && a>=10);
        System.out.println(a>=5 || a>=10);
    }  */


// HOW COMPILER PERFORM ACTION ON OPERATOR
   /*  public static void main(String[] args) {
       float a = 4;
       float b = 2;

       float c = a/b-b/a+a-b*a;
       float d = a*b-b/a+a-b/a;
       System.out.println(c);
       System.out.println(d);
   } */


//INCREMENT AND DECREMENT OPERATOR
 /* public static void main(String[] args) {
      int a = 55;
      System.out.println(a++);
      System.out.println(a);
      System.out.println(++a);
      System.out.println(a);

      char c = 'f';
      c++;              // it prints next character
      System.out.println(c);
  }  */

//exercise 1 question 1
 /*public static void main(String[] args) {
     Scanner sc = new Scanner(System.in);
     System.out.println("what is your grade");
     int grade = sc.nextInt();
     grade += 8;
     System.out.println("my real grade is " +grade);
 }
                    //Another method
 public static void main(String[] args) {
     char grade = 'a';
     grade = (char)(grade + 8);
     System.out.println(grade);
     //decripting the grade
     grade = (char)(grade - 8);
     System.out.println(grade);

 }*/

//METHODS IN STRINGS
 /*public static void main(String[] args) {
    String name = "AshisH";
    int value = name.length();
        System.out.println(value);

        System.out.println(name.toLowerCase());  // convert to lower case
        System.out.println(name.toUpperCase()); // convert to upper case
        System.out.println(name.trim());    //remove blank spaces
        System.out.println(name.substring(4));  // only print from index 4
        System.out.println(name.substring(2,5)); // only print between index 2,5   5 is excluded
        System.out.println(name.replace("s","p"));    // replace character from s to p
        System.out.println(name.startsWith("Ash"));   // says true or false
        System.out.println(name.endsWith("isH"));     // says true or false
        System.out.println(name.charAt(3)); // print character on that index
        System.out.println(name.indexOf("hi")); //return value of the index
        System.out.println(name.indexOf("s",3));  //search index of s from the index after 3
        System.out.println(name.lastIndexOf("sh")); // print last sh of the string
        System.out.println(name.equals("AshisH"));  // checks the entered string is true or not
        System.out.println(name.equalsIgnoreCase("ASHish")); // ingore upper case in this method


        System.out.println("My name is \"Ashish patil");  //print double coat
        System.out.println("My father name is\n Vishwanath patil");  // create New line
        System.out.println("MY home is at\t pithampur");  // create a tab like space between two characters
        System.out.println("I am a BCA \'student\'");   // print single coat
 }*/

//Practice set 4
//String name = "AsHiSh PaTiL";
//Problem 1
            // System.out.println(name.toLowerCase());

//Problem 2
            // System.out.println(name.replace(" ","_"));

//Problem 3
        /* Scanner sc = new Scanner(System.in);
        String letters = "My <|name|> is Ashish";
        String letter = sc.nextLine();
        System.out.println("Dear "+ letter + " thanks a lot");
        System.out.println(letters.replace("<|name|>","Original name"));*/

//Problem 4
       /* String Ash = "My Name  Is   Ashish";
        System.out.println(Ash.indexOf("  "));
        System.out.println(Ash.indexOf("   "));  */

//Problem 5
     /*   String letter = "Dear Ashish,\n \t This Java Cousce is nice,\n \tThanks";
        System.out.println(letter);   */


//IF ELSE STATEMENT
/*public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    System.out.println("enter your age");
    int age = sc.nextInt();
    if(age>18)                        // age is greater than 18
    if(age!=18)                   // age is not equal to 18
    if(age==18)               // age is equal to  18
    if(age>=18)           // age is greater than equal to 18
                {
                    System.out.println("yes boy you can drive");
                }
                else{
                    System.out.println("no boy you can't drive it");
                }   */

//BOOLEAN AND REALATIONAL OPERATORS
  /*public static void main(String[] args) {
        boolean a = false;
        boolean b = true;
        boolean c = false;
        if(a && b || c){            //&& is and ,|| is or  , !a is not a
            System.out.println("yes");
    }
else{
            System.out.println("no");
        }

    }*/

//IF ELSE STATEMENT AND ELSE IF STATEMENT
  /*  public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("enter your age ");
        int age = sc.nextInt();
        if(age>56){
            System.out.println("you are experienced");
        }
        else if(age>46){
            System.out.println("you are semi experienced");
        }
        else if(age>36){
            System.out.println("you are semi semi experienced");
        }
        else{
            System.out.println("you are not experienced");
        }
        if(age>2){
            System.out.println("you are not a baby");
        }
    }*/

//SWITCH CASE STATEMENT AND BREAK STATEMENT
   /* public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("enter your age ");
        int age = sc.nextInt();
        switch(age){
            case 18:
                System.out.println("you are become an adult");
                break;

            case 23:
                System.out.println("you are going to get a job");
                break;

            case 60 :
                System.out.println("you are getting retired ");
                break;

            default:
                System.out.println("you are enjoying");
        }
        System.out.println("thanks for using java code");
    }*/



            //  public static void main(String[] args) {
//problem 1
    /*    int a = 10;
        if(a==11){
            System.out.println("I am 11");
        }
        else{
            System.out.println("i am not 11");
        }*/

//Problem 2
      /*  Scanner sc = new Scanner(System.in);
        System.out.println("enter the marks of the three subjects");
        int a = sc.nextInt();
        int b = sc.nextInt();
        int c = sc.nextInt();
        int per = (a+b+c)/3;

        if(per>40 && a>33 && b>33 && c>33){
            System.out.println("the student is pass");
        }
        else {
            System.out.println("the student is fail");
        }*/

//problem 4
    /*    System.out.println("enter sales of your company");
        Scanner sc = new Scanner(System.in);
        float a = sc.nextFloat();
        if(a<2.5){
            System.out.println("you don't need to pay the income Tax");
        }
       else if(a>=2.5f && a<=5.0f){
            float tax1 = (a*5)/100;
            System.out.println("your income tax will be " + tax1);

        }
        else if(a>=5.0f && a<=10.0f){
            float tax2 = (a*5)/100 + (a*20)/100;
            System.out.println("your income tax will be " + tax2);

        }
        else{
            float tax3 = (a*5)/100 + (a*20)/100 + (a*30)/100;
            System.out.println("your income tax will be " + tax3);
        }*/

//Problem 4
    /*    System.out.println("enter the number");
        Scanner sc = new Scanner(System.in);
        int a = sc.nextInt();

        switch(a){
            case 1:
                System.out.println("monday");
                break;
            case 2:
                System.out.println("Tuesday");
                break;
            case 3:
                System.out.println("Wednesday");
                break;
            case 4:
                System.out.println("Thrusday");
                break;
            case 5:
                System.out.println("Friday");
                break;
            case 6:
                System.out.println("saturday");
                break;
            case 7:
                System.out.println("Sunday");
                break;

            default:
                System.out.println("please enter number between 1 to 7 only");
        }

        //we also write this like
        switch(a){
            case 1 -> System.out.println("monday");
            case 2 -> System.out.println("Tuesday");
            case 3 -> System.out.println("Wednesday");
            case 4 -> System.out.println("Thrusday");
            case 5 -> System.out.println("Friday");
            case 6 -> System.out.println("Saturday");
            case 7 -> System.out.println("Sunday");
        } */

//problem 5
    /*    System.out.println("enter a year");
        Scanner sc = new Scanner(System.in);
        float a = sc.nextInt();
        float b = 4;

        float year = a%b;
        System.out.println(year);
        if(year==0){
            System.out.println("the year is a leap year");

        }
        else{
            System.out.println("the year is not a leap year");
        }  */

//problem 6
     /*   System.out.println("enter the url of the website");
        Scanner sc = new Scanner(System.in);
        String name = sc.nextLine();
        boolean a = name.endsWith("com");
        boolean b = name.endsWith("org");
        boolean c = name.endsWith("in");

        if(a==true){
            System.out.println("the website is commercial website");
        }
        else if(b==true){
            System.out.println("the website is organisational website");

        }else if(c==true){
            System.out.println("the website is indian website");
        }
        else
        {
            System.out.println("please enter a valid website url");
        }

}
*/






    }
}
