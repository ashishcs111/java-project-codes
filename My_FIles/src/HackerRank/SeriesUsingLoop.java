package HackerRank;
import java.util.*;
import java.lang.*;
public class SeriesUsingLoop {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int q = sc.nextInt();
      for(int z=0;z<q;z++)
      {
          int a = sc.nextInt();
          int b = sc.nextInt();
          int n = sc.nextInt();
          double ad = 2, sum = 0;
              for (int i = 0; i < n; i++)
              {
                  for (double j = 0; j < ad; i++)
                  {
                      sum = sum + (a + (Math.pow(2, j) * b));
                  }
                  System.out.println(sum);
                  ad = ad + 1;
              }
      }
    }
}
