import java.util.Scanner;

public class AlphabetOrNot {
    public static void main(String[] args) {
       Scanner sc = new Scanner(System.in);
       char ch= sc.next().charAt(0);
       if((int)ch>=65 && (int)ch<=90 || (int)ch>=97 && (int)ch<=122)
       {
           System.out.println("the entered value is character");
       }
       else
       {
           System.out.println("the entered value is not a character");
       }
    }
}
