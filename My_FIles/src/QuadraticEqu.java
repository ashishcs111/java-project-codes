import java.lang.Math;
public class QuadraticEqu {
    public static void main(String[] args)
    {
        double a = 12, b = 5,c = 4;
        double x1 = (-b-(java.lang.Math.sqrt(b*b-4*a*c)))/2*a;
        double x2 = (-b+(java.lang.Math.sqrt(b*b-4*a*c)))/2*a;
        System.out.println("one root of the quadratic equation is "+ x1);
        System.out.println("other root of the quadratic equation is  "+ x2);
        System.out.println(java.lang.Math.sqrt(4));
    }
}
