package Assignemnts;
import java.util.Scanner;
public class PrintTableOfDigit {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int r,i;
        System.out.println("Enter a number : -");
        int num = sc.nextInt();
        while(num>0)
        {
            r = num%10;
            for(i=1;i<=10;i++) {
                System.out.println(r + "x" + i + " " +(r*i));
            }
            System.out.println("***********************************************");
            num=num/10;
        }
    }
}
