package AppPrograms;

import java.util.Scanner;

public class AllAppPrograms {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        /*
        PRINT ALL PRIME NUMBERS
        int i,j,a=0;
        int num = sc.nextInt();
        System.out.println("2");
        for(i=3;i<num;i++)
        {
            a=0;
            for(j=2;j<i;j++)
            {
                if(i%j==0)
                {
                    a=a+1;
                }
            }
            if(!(a>1))
            {
                System.out.println(i);
            }
        }

         */

        /*

        MAXIMUM AND MINIMUM NUMBER IS AN ARRAY
        int arr[] = new int[10];
        int i,max=0;
        for(i=0;i<5;i++)
        {
            arr[i]=sc.nextInt();
        }
        for(i=0;i<5;i++)
        {
            if(arr[i]>max)
            {
                max = arr[i];
            }
        }
        int min=max;
        for(i=0;i<5;i++)
        {
            if(arr[i]<min)
            {
                min=arr[i];
            }
        }
        System.out.println("Maximum number is "+max);
        System.out.println("minimum number is "+min);

         */

        /*
        DATE AND TIME PROGRAM
        Date d = new Date();
        SimpleDateFormat f1 = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat f2 = new SimpleDateFormat("E");
        SimpleDateFormat f3 = new SimpleDateFormat("hh:mm;ss a");
        SimpleDateFormat f4 = new SimpleDateFormat("MMM");

        System.out.println("The current Date "+f1.format(d));
        System.out.println("today's Day is "+f2.format(d));
        System.out.println("Current Time is "+f3.format(d));
        System.out.println("Current Month is "+ f4.format(d));
*/

        /*
        FIND THE NUMBER IS A PERFECT SQUARE OR NOT
        int num = sc.nextInt();
        int i;
        for(i=1;i<num;i++)
        {
            if(i*i==num)
            {
                System.out.println("number is a perfect Square");
                break;
            }
        }
        if(i==num)
        {
            System.out.println("Number is not a perfect squarre");
        }

         */

        /*
        Convert Decimal into binary
        System.out.println("Enter a number");
        int num = sc.nextInt();
        int ar[]= new int[10];
        int i,c=0;
        for(i=0;i<10;i++)
        {
           ar[i]=num%2;
           num = num/2;
           c++;
           if(num==1)
           {
               ar[i+1]=num;
               break;
           }
        }
        for(i=c;i>=0;i--)
        {
            System.out.print(ar[i]);
        }

         */


        /*
        CONVERT STRING TO OTHER DATA TYPES
        int num1 = sc.nextInt();
        String str="101";
        String str1="33.99";
        String str2="true";
        int num2 = Integer.parseInt(str);
        int sum = num1 + num2;
        double d = Double.parseDouble(str1);
        boolean b = Boolean.parseBoolean(str2);
        System.out.println(sum);
        System.out.println("The double is "+d);
        System.out.println("The Boolean is "+b);

         */

        /*
        STRING TO CHARACTER ARRAY
        String str = "My name is Ashish";
        char ar[]= new char[30];
        int i;
        for(i=0;i<str.length();i++)
        {
            ar[i]=str.charAt(i);
            System.out.print(ar[i]);
        }

         */

        /*
        CALCULATOR
        float a,b;
        do {
            System.out.println("1. For Addition");
            System.out.println("2. For Substraction");
            System.out.println("3. For Multiplication");
            System.out.println("4. For Division");
            System.out.println("5. For Modulus");
            System.out.println("6. For Exit");
            System.out.println("Enter Your Choice");
            int ch = sc.nextInt();

            switch (ch) {
                case 1:
                    System.out.println("enter Two Numbers");
                    a = sc.nextFloat();
                    b = sc.nextFloat();
                    System.out.println("Your Addition is " + (a + b));
                    break;
                case 2:
                    System.out.println("enter Two Numbers");
                     a = sc.nextFloat();
                     b = sc.nextFloat();
                    System.out.println("Your Addition is " + (a-b));
                    break;
                case 3:
                    System.out.println("enter Two Numbers");
                    a = sc.nextFloat();
                    b = sc.nextFloat();
                    System.out.println("Your Addition is " + (a * b));
                    break;
                case 4:
                    System.out.println("enter Two Numbers");
                    a = sc.nextFloat();
                    b = sc.nextFloat();
                    System.out.println("Your Addition is " + (a / b));
                    break;
                case 5:
                    System.out.println("enter Two Numbers");
                    a = sc.nextFloat();
                    b = sc.nextFloat();
                    System.out.println("Your Addition is " + (a % b));
                    break;
                case 6:
                    System.exit(0);
                default:
                    System.out.println("Enter a valid Choice");
            }
        }while(true);

         */

        /*
        FIND ARITHMETIC MEAN
        System.out.println("Enter total number of data");
        int num = sc.nextInt();
        System.out.println("Enter data ");
        int a[] = new int[10];
        int i,sum=0;
        for(i=0;i<num;i++)
        {
            a[i]= sc.nextInt();
            sum = sum+a[i];
        }
        float mean = sum/num;
        System.out.println("Mean of following data is "+mean);

         */

        



    }
}
