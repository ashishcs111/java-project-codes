package AppPrograms;

public class PascalTriangle {
    public static void main(String[] args) {
        int sp=4;
        for(int i =1;i<=5;i++)
        {
            for(int k=sp;k>=1;k--)
            {
                System.out.print(" ");
            }
            for(int j=1;j<=i;j++)
            {
                System.out.print(i+(i-1));
                System.out.print(" ");
            }
            System.out.println();
            sp=sp-1;
        }
    }
}
