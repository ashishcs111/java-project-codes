package javaClass;

public class CopyingAnArray {
    public static void main(String[] args) {
        int A[]={8,6,10,9,2,15,7,13,14,11};
        int B[]=new int[10];
        for(int i=0;i<A.length;i++)
        {
            B[i]=A[i];
        }
        for(int i=0;i<B.length;i++)
        {
            System.out.println(B[i]);
        }

        System.out.println();
        System.out.println();

        //reverse copying an array
        int A1[]={8,6,10,9,2,15,7,13,14,11};
        int B1[]=new int[10];
        int c=0;
        for(int i=A1.length-1;i>=0;i--)
        {
            B1[c]=A1[i];
            c++;
        }
        for(int i=0;i<B1.length;i++)
        {
            System.out.println(B1[i]);
        }
    }
}
