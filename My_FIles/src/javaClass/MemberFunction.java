package javaClass;
public class MemberFunction {
    static int getCode() {
        return 1234;
    }
    int getCode1()
    {
        return 12345;
    }

    public static void main(String ap[])
    {
        MemberFunction d = new MemberFunction();
            //static method does not need object to print
        System.out.println(getCode());
        //non-static method need object to display it's content
        System.out.println(d.getCode1());
    }
}
