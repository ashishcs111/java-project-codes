package javaClass;
import java.util.ArrayList;
import java.util.List;
public class ArrayList1 {
    public static void main(String[] args) {
        List<Integer> l = new ArrayList<>();
        for(int i=0;i<5;i++)
        {
            l.add(i);
        }
        System.out.println(l);
        l.add(14);
        System.out.println(l);
        l.set(0,10);
        System.out.println(l);

        System.out.println(l.get(5));
    }
}
