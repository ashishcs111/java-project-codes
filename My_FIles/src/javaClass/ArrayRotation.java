package javaClass;

public class ArrayRotation {
    public static void main(String[] args) {
        int A[]={5,9,6,10,12,7,3,5,4,2};
        int B[]={5,9,6,10,12,7,3,5,4,2};
        //Rotate array at left
        int temp=A[0];
        for(int i=0;i<A.length-1;i++)
        {
            A[i]=A[i+1];
        }
        A[A.length-1]=temp;
        for(int i=0;i<A.length;i++)
        {
            System.out.println(A[i]);
        }



        System.out.println();
        System.out.println();



        //Rotate array at right
        int temp1=B[B.length-1];
        for(int i=B.length-1;i>0;i--)
        {
            B[i]=B[i-1];
        }
        B[0]=temp1;
        for(int i=0;i<B.length;i++)
        {
            System.out.println(B[i]);
        }
    }
}
