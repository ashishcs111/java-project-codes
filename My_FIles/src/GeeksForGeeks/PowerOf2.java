package GeeksForGeeks;
import javax.swing.*;
import java.util.Scanner;
import java.lang.*;
public class PowerOf2 {
   public static boolean isPowerofTwo(long n){
       int c=0;
       for(int i=0;i<100000;i++)
       {
           if(n==Math.pow(2,i))
               c=c+1;
          if(c==1)
          {
              return true;
          }
          else
          {
              return false;
          }
       }
       return false;
   }
    public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    long N=sc.nextLong();
    PowerOf2 p = new PowerOf2();
    System.out.println(p.isPowerofTwo(N)==true?"YES":"NO");
    }
}
