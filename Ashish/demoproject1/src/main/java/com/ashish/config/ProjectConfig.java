package com.ashish.config;

import com.ashish.beans.Vehicle;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "com.ashish.beans")
public class ProjectConfig {

    /*@Bean
    public Vehicle vehicle(){
        Vehicle veh = new Vehicle();
        veh.setName("Audi 8");
        return veh;
    }*/

    @Bean(name = "hello")
    String hello() {
        return "Hello world";
    }

    @Bean
    Integer number(){
        return 19;
    }

    @Bean(name = "welcome")
    String welcome(){
        return "welcome";
    }
}


