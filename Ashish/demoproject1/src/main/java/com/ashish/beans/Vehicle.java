package com.ashish.beans;

import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

@Component
public class Vehicle {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @PostConstruct
    public void postconst(){
        System.out.println("post construct is running");
    }

    @PreDestroy
    public void preDest(){
        System.out.println("pre destroy is running");
    }
}
