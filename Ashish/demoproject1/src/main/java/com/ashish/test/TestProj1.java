package com.ashish.test;

import com.ashish.beans.Vehicle;
import com.ashish.config.ProjectConfig;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class TestProj1 {
    public static void main(String[] args) {

//        Vehicle vehicle = new Vehicle();
//        vehicle.setName("Honda City");
//        System.out.println("vehicle name from non-spring contest is : "+ vehicle.getName());


        ApplicationContext context = new AnnotationConfigApplicationContext(ProjectConfig.class);
        Vehicle veh = context.getBean(Vehicle.class);
        System.out.println("vehicle name from Spring context is : "+ veh.getName());

        String hello = context.getBean("welcome",String.class);
        Integer num = context.getBean(Integer.class);
        System.out.println("integer and spring value from spring contest is : "+ hello + " "+ num);


    }
}
