public class Interview {
    public void fib(int a,int b,int key){
        for(int i=0;true;i++)
        {
            int temp = a+b;
            a=b;
            b=temp;
            if(temp==key) {
                System.out.println("Key is present in the Fibonacci series");
                break;
            }
            else if(key<temp) {
                System.out.println("Key is not present in the Fibonacci series");
                break;
            }
        }
    }
    public static void main(String[] args) {
        Interview i = new Interview();
        i.fib(0,1,13);
    }
}
