public class ArmstrongNumber {
    public boolean isArmstrong(int num){
        int length = Integer.toString(num).length();
        int sum=0;
        int temp=num;
        while(num>0)
        {
            int r= num%10;
            sum = sum+(int)Math.pow(r,length);
            num= num/10;
        }
        if(temp==sum)
            return true;
        return false;

    }
    public static void main(String[] args) {
        ArmstrongNumber an = new ArmstrongNumber();
        int num=153;
        System.out.println(an.isArmstrong(num));
    }
}
