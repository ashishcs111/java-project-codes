public class StringPalindrome {
    public StringBuilder isStringPalindrome(String str){
        return new StringBuilder(str).reverse();
    }
    public static void main(String[] args) {
        String str = "my self ashish patil";
        StringPalindrome sp = new StringPalindrome();
        System.out.println(sp.isStringPalindrome(str));
    }
}
