public class IntegerPalindrome {

    public boolean isIntegerPalindrome(int num){
        int sum=0;
        int temp=num;
        while(num>0)
        {
            int r= num%10;
            sum = sum*10+r;
            num= num/10;
        }
        if(temp==sum)
            return true;
        return false;

    }
    public static void main(String[] args) {
        IntegerPalindrome ip = new IntegerPalindrome();
        int num = 124321;
        System.out.println(ip.isIntegerPalindrome(num));
    }
}
